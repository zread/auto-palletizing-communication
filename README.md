# Auto Palletizing README

### Created
January 2015

### Created By
Zach Read

### Purpose
This program is responsible for determining which box a panel should be sent to by the packbot. It does this by getting the power class of that panel and checking to see if the panel is A-,B,F or A.
It is used by the packbot.

This program also marks EL 2 rejects as A-.

This program interfaces with the PLC through KepServerEX 5. See communication diagram for more information.

### Setup
This program has to be run as a windows service. It is currently installed on server CA01A0046.

Runs in conjuction with a a Python script which communicates with the OPC server. 
Pyhton script is called Palletizing_OPC_Comm.py and is located under C:\Auto_Palletizing_Wattage\ on the server CA01A0046
The two programs communicate using a text file called Communication.txt which is located in the same folder.
Relies on Packaging server (CA01s0015).

To setup a windows service run the following command in command line. (Ensure you have opened it as the administrator.)
C:\Windows\Microsoft.NET\Framework\v4.0.30319\installutil.exe -u "C:\Services\myservice.exe" (Replace The last path with the programs path and name.)

### Deployed
Runs as a service on CA01a0046.
Service name is Auto_Palletizing_Comm.
Running program is located in C:\Auto_Palletizing_Wattage\Auto_Palletizing_Comm.exe on server ca0a0046.

### Contribution guidelines
If changes are deployed they will immediately affect the line.  

### Who do I talk to?
Zach Read is the current owner of this project.