# Updated July 26th, 2017: Added Line A Packbot and Removed Line D Packbot and Auto Printer

import OpenOPC
import time
import sys
import os
import datetime
opc = OpenOPC.client()

#EOL AutoPrinter Function
def readAPFile(printRead, Line):
    #Read from communication file

    with open("C:\OPC_Comm\Sta510SN" + Line + ".txt", 'r') as f:
        filelines = sum(1 for _ in f)
    with open("C:\OPC_Comm\Sta510SN" + Line + ".txt", 'r') as f:
        if(filelines > 2 and filelines < 5):
            success = f.readline()
            printed = f.readline()
            verified = f.readline()
        
            try:
                #Set Reject Counter
                opc.write(("Line " + Line + " Kuka.Kuka 01.LN" + Line + "KK_AutoprinterCounter", success))
              
                #Checks if label has been printed
                if int(printed) > 0:
                    opc.write(("Line " + Line + " Kuka.Kuka 01.LN" + Line + "KK_Frame_Printed", 1))

                #ts = time.time()
                #testings = open("C:\Auto_Palletizing_Wattage\TestA.txt", 'a')
                #testings.write(datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S') + ": " + str(verified))
                #testings.write("\n")                
        
                #Check if labels at 520 have been verified
                if int(verified) == 0:
                    opc.write(("Line " + Line + " Kuka.Kuka 01.LN" + Line + "KK_Barcode_Read", 0))
                    #testings.write(datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S') + ": " + "Overwrote")
                    #testings.write("\n")                  

                #testings.close()

                # Write back communication file
                target = open("C:\OPC_Comm\Sta510SN" + Line + ".txt", 'w')                
                target.write(str(int(printRead[0][1])))
                target.write("\n")
                target.write(str(int(printRead[1][1])))
                target.write("\n")
                target.write(str(int(printRead[2][1])))               
                target.write("\n")
                target.write(str(printRead[3][1]))                
                target.write("\n")
                target.write(str(int(printRead[4][1])))
                target.close()
            
            except OpenOPC.TimeoutError:
                print "TimeoutError occured Printer" + Line

    return

def readAutoPallet():    
    filesize = os.path.getsize("C:\Auto_Palletizing_Wattage\Communication.txt")
    gotWattageA = True
    gotWattageB = True

    if(filesize > 1):
        with open("C:\Auto_Palletizing_Wattage\Communication.txt", 'r') as target:
            wattageA = target.readline()
            wattageB = target.readline()
            rejectAckA = target.readline()
            rejectAckB = target.readline()
        
        try:
            try: 
                int(wattageA)
                int(wattageB)
                valid = True
            except ValueError:
                valid = False
                #print "Value Error Wattage"
            if valid: 
                #print wattage
                palletRead = opc.read(palletTags)
                
                writeToA = str(palletRead[4][1])
                writeToB = str(palletRead[5][1])
                
                if str(palletRead[4][1]) == "65535":
                    writeToA = "-1"
                if str(palletRead[5][1]) == "65535":
                    writeToB = "-1"
                    
                if int(wattageA) > -1:
                    gotWattageA = opc.write(("Line A Kuka.Kuka 01.LNAKK_Sta500_Temp_Wattage", wattageA))                    

                if int(wattageB) > -1:
                    gotWattageB = opc.write(("Line B Kuka.Kuka 01.LNBKK_Sta500_Temp_Wattage", wattageB))

                if int(rejectAckA) == 3:
                    gotWattageA = opc.write(("Line A Kuka.Kuka A.LNAKK_Sta200_RejectAck", True))

                if int(rejectAckB) == 3:
                    gotWattageB = opc.write(("Line B Kuka.Kuka B.LNBKK_Sta200_RejectAck", True))
                               
                target = open("C:\Auto_Palletizing_Wattage\Communication.txt", 'w')
                target.write(str(palletRead[6][1]))     # Line A STA 500 SN
                target.write("\n")
                target.write(str(palletRead[7][1]))     # Line B STA 500 SN 
                target.write("\n")
                target.write(str(palletRead[8][1]))     # Line A STA 510 SN
                target.write("\n")
                target.write(str(palletRead[9][1]))     # Line B STA 510 SN
                target.write("\n")
                target.write(str(palletRead[10][1]))    # Line A STA 530 SN
                target.write("\n")
                target.write(str(palletRead[11][1]))    # Line B STA 530 SN
                target.write("\n")
                target.write(writeToA)                  # Line A Wattage
                target.write("\n")
                target.write(writeToB)                  # Line B Wattage
                target.write("\n")
                target.write(str(gotWattageA))          # Success of writing line A wattage to OPC
                target.write("\n")
                target.write(str(gotWattageB))          # Success of writing line B wattage to OPC
                target.write("\n")
                if int(palletRead[0][1]) == 2:          # Write line A STA 200 SN if panel is rejected
                    target.write(str(palletRead[2][1]))
                else:
                    target.write(" ")
                target.write("\n")
                if int(palletRead[1][1]) == 2:          # Write line B STA 200 SN if panel is rejected
                    target.write(str(palletRead[3][1]))
                else:
                    target.write(" ")
                target.close()
        
        except OpenOPC.TimeoutError:
            print "TimeoutError occured Pallet"
        except ValueError:
            print "Value Error WriteToD"

    return

opc.connect("Kepware.KEPServerEX.V5")
# Pallet Tags
palletTags = [  "Line A Kuka.Kuka A.LNAKK_EL1_Decision",                #0
                "Line B Kuka.Kuka B.LNBKK_EL1_Decision",                #1
                "Line A Kuka.Kuka A.LNAKK_Sta200_Serial_Number",        #2
                "Line B Kuka.Kuka B.LNBKK_Sta200_Serial_Number",        #3
                "Line A Kuka.Kuka 01.LNAKK_Sta500_Temp_Wattage",        #4
                "Line B Kuka.Kuka 01.LNBKK_Sta500_Temp_Wattage",        #5
                "Line A Kuka.Kuka 01.LNAS500Serial_Number",             #6
                "Line B Kuka.Kuka 01.LNBS500Serial_Number",             #7               
                "Line A Kuka.Kuka 01.LNAS510Serial_Number",             #8
                "Line B Kuka.Kuka 01.LNBS510Serial_Number",             #9
                "Line A Kuka.Kuka 01.LNAS530Serial_Number",             #10
                "Line B Kuka.Kuka 01.LNBS530Serial_Number"]             #11


# Printer Tags
# printerTagsA = ["Line A Kuka.Kuka 01.LNAKK_AutoPrinterReset",
#               "Line A Kuka.Kuka 01.LNAKK_Barcode_Read",
#               "Line A Kuka.Kuka 01.LNAKK_AutoLabel",
#               "Line A Kuka.Kuka 01.LNAS520Serial_Number",
#               "Line A Kuka.Kuka 01.LNAKK_Frame_Printed"]

# Printer Tags
#printerTagsB = ["Line B Kuka.Kuka 01.LNBKK_AutoPrinterReset",
#               "Line B Kuka.Kuka 01.LNBKK_Barcode_Read",
#               "Line B Kuka.Kuka 01.LNBKK_AutoLabel",
#               "Line B Kuka.Kuka 01.LNBS520Serial_Number",
#               "Line B Kuka.Kuka 01.LNBKK_Frame_Printed"]

# Printer Tags
#printerTagsD = ["Line D Kuka.Kuka 01.LNDKK_AutoPrinterReset",
#               "Line D Kuka.Kuka 01.LNDKK_Barcode_Read",
#               "Line D Kuka.Kuka 01.LNDKK_AutoLabel",
#               "Line D Kuka.Kuka 01.LNDS520Serial_Number",
#               "Line D Kuka.Kuka 01.LNDKK_Frame_Printed"]

opc.read(palletTags, group='Pallet')
#opc.read(printerTagsA, group='PrinterA')
#opc.read(printerTagsB, group='PrinterB')
#opc.read(printerTagsD, group='PrinterD')



while True:
    #readAPFile(opc.read(printerTagsA), 'A')
    #readAPFile(opc.read(printerTagsB), 'B')
    #readAPFile(opc.read(printerTagsD), 'D')
    readAutoPallet()
    time.sleep(1)

opc.Remove('Pallet')
#opc.Remove('PrinterA')
#opc.Remove('PrinterB')
#opc.Remove('PrinterD')
opc.close
sys.exit()
