﻿/*
 * Created by SharpDevelop.
 * User: zach.read
 * Date: 25-08-15
 * Time: 10:16
 * 
 */

namespace Auto_Palletizing_Comm
{
    partial class OPC_Service
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            // 
            // OPC_EOL_Service
            // 
            this.ServiceName = "Auto_Palletizing_Comm";
        }

        #endregion
    }
}
