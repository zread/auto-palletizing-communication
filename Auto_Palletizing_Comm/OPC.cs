﻿/* 
 * User: Zach Read
 * Date: 03-02-16
 * Purpose: This method manages the communication between the packaging system and the PLC.
 * This windows service manages the communication between the packaging system and the PLC through communication with the OPC server.
 * 
 */

using System;
using System.IO;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Data;

namespace Auto_Palletizing_Comm
{
    class OPC
    {
		private static String connectionString = "Data Source= 10.127.34.15; Initial Catalog= CSILabelPrintDB;User ID=EOLAutoPrinter;Password=solarEOL";
		private static String connectionStringNP = "Data Source= 10.127.34.15; Initial Catalog= LabelPrintDB;User ID=EOLAutoPrinter;Password=solarEOL";		
		
		// Reads from the packaging system and python script output
		// Provides data back to packaging system and python script
    	public static void ReadData(){
    		StreamReader sr = null;
    		StreamWriter sw = null;
    		String pythonComm = AppDomain.CurrentDomain.BaseDirectory + "\\Communication.txt";
    		String Tracker = AppDomain.CurrentDomain.BaseDirectory + "\\Tracker.txt";
    		var lines = new String[8];
    		var lastSN = new String[2];			
			var outputA = new string[2];
			var outputB = new string[2];			
			int rejectAckA = 0;
			int rejectAckB = 0;
			
			var random = new Random();
			int randomNum = random.Next(0, 8000);
			
			Process [] proc;
			proc = Process.GetProcessesByName("python");
			if(proc.Length == 0){
				Process.Start(@"C:\Auto_Palletizing_Wattage\Palletizing_OPC_Comm.py");				
				randomNum = 0;
				Library.WriteErrorLog("Python OPC failed, restarted successfully.");
			}
			
			if(randomNum == 333){
				try{
					Library.WriteErrorLog("Successfully killed Python OPC");
					try{    			
						sw = new StreamWriter(pythonComm, false);
						sw.WriteLine("-1");
						sw.WriteLine("-1");
						sw.WriteLine(0);				
						sw.WriteLine(0);
						sw.Close();
		    		}
		    		catch{
		    			Library.WriteErrorLog("Error Writing Communication text file.");
		    			Library.WriteLastSN("");
    				}					
										
					proc[0].Kill();
					Process.Start(@"C:\Auto_Palletizing_Wattage\Palletizing_OPC_Comm.py");
				}
				catch(Exception ex){
					Library.WriteErrorLog("Failed to restart Python OPC.");
					Process.Start(@"C:\Auto_Palletizing_Wattage\Palletizing_OPC_Comm.py");
    				Library.WriteErrorLog(ex);
				}				
			}				
			
    		try{
    			sr = new StreamReader(Tracker);
    			lastSN = sr.ReadToEnd().Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
    			sr.Close();
    		}
    		catch(Exception ex){
    			Library.WriteErrorLog("Error Reading Tracker text file.");
    			Library.WriteErrorLog(ex);
    		}
    		try{
    			sr = new StreamReader(pythonComm);
    			lines = sr.ReadToEnd().Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
    			sr.Close();
    		}
    		catch(Exception ex){
    			Library.WriteErrorLog("Error Reading Communication text file.");
    			Library.WriteErrorLog(ex);
    		}
						
			/* Data from Palletizing_OPC_Comm.py
			 * 
    		 * lines[0]  	- Line A 500 SN
    		 * lines[1]  	- Line B 500 SN
    		 * lines[2]  	- Line A 510 SN
    		 * lines[3]  	- Line B 510 SN
    		 * lines[4]  	- Line A 530 SN
    		 * lines[5]  	- Line B 530 SN
    		 * lines[6]		- Status of line A bin number
    		 * lines[7] 	- Status of line B bin number
    		 * lines[8] 	- Success of writing to OPC server Line A
    		 * lines[9] 	- Success of writing to OPC server Line B
    		 * lines[10] 	- SN From Sta 200 Line A
    		 * lines[11] 	- SN From Sta 200 Line B
    		 */			
   			if(lines[0] != "-1")
	    		sqlConn("A", "Sta 500", lines[0]);
   			if(lines[1] != "-1")
	    		sqlConn("B", "Sta 500", lines[1]);
			if(lines[2] != "-1")
	    		sqlConn("A", "Sta 510", lines[2]);
    		if(lines[3] != "-1")
	    		sqlConn("B", "Sta 510", lines[3]);
			if(lines[4] != "-1")
	    		sqlConn("A", "Sta 530", lines[4]);
			if(lines[5] != "-1"){
	    		sqlConn("B", "Sta 530", lines[5]);
	    		if(!String.IsNullOrEmpty(lines[5])){
	    			sqlELImageTracker(lines[5]);
	    		}	    		
			}
    		
			//Finds the wattage for each serial number
			outputA = checkLine(lines[0].ToString().Trim(), lastSN[0].ToString().Trim(), lines[6].ToString().Trim(), lines[8].ToString().Trim());
			outputB = checkLine(lines[1].ToString().Trim(), lastSN[1].ToString().Trim(), lines[7].ToString().Trim(), lines[9].ToString().Trim());			
			
			//Checks to see if we need to mark the panel at station 200 to be A- if it was rejected
			//The number 3 is used on the OPC server to send the Acknowledge signal back to the PLC
			if(lines[10].ToString().Trim() != ""){
				if(sqlGetRemark(lines[10].ToString().Trim()) != "Auto KUKA EL Reject"){					
					sqlAMinus("A", "200", lines[10].ToString().Trim(), "Auto KUKA EL Reject");
					rejectAckA = 3;
					Library.WriteErrorLog("Set SN: " + lines[10].ToString().Trim() + " to A-.");
				}
			}
			
			//Checks to see if we need to mark the panel at station 200 to be A- if it was rejected
			//The number 3 is used on the OPC server to send the Acknowledge signal back to the PLC
			if(lines[11].ToString().Trim() != ""){
				if(sqlGetRemark(lines[11].ToString().Trim()) != "Auto KUKA EL Reject"){					
					sqlAMinus("B", "200", lines[11].ToString().Trim(), "Auto KUKA EL Reject");
					rejectAckB = 3;
					Library.WriteErrorLog("Set SN: " + lines[10].ToString().Trim() + " to A-.");
				}
			}
			
			// Writes the wattage to the OPC server
			
			try{    			
				sw = new StreamWriter(pythonComm, false);
				sw.WriteLine(outputA[0]);
				sw.WriteLine(outputB[0]);
				sw.WriteLine(rejectAckA);				
				sw.WriteLine(rejectAckB);
				sw.Close();
    		}
    		catch{
    			Library.WriteErrorLog("Error Writing Communication text file.");
    			Library.WriteLastSN("");
    		}
			
			// Writes the last serial number to the textfile
			Library.WriteLastSN(outputA[1]);
			Library.WriteLastSNAppend(outputB[1]);
		}

		public static string[] checkLine(string sn, string lastSN, string commStr, string success){
			string sqlData = "", wattage = "", sqlClassData = "";
			var output = new string[2];
			output[0] = "";
			output[1] = "";
					
			if(commStr == "-1" && sn.Length > 12 && (sn != lastSN || success == "False")){
				// Reads data from packaging system
				sqlData = sqlGetData(sn);
				wattage = getIdealPower(sqlData, sn);
				sqlClassData = sqlGetClass(sn);
				Library.WriteErrorLog(sn + ", " + sqlData.ToString() + ", " + sqlClassData.ToString() + ", " + wattage);
				
	    		// Writes data back to the OPC server
	    		// Checks to see if the query is successful
	    		if (!String.IsNullOrEmpty(sqlData) && (String.IsNullOrEmpty(sqlClassData) || sqlClassData == "A")){
	    			output[0] = wattage;
	    			output[1] = sn;
	    		}
	    		else if(!String.IsNullOrEmpty(sqlClassData) && sqlClassData != "A"){
	    			output[0] = "0";
	    			output[1] = sn;
	    		}
	    		else{
	    			output[0] = "-1";
	    			output[1] = lastSN;
	    		}
    		}
			else{
				output[0] = "-1";
				output[1] = lastSN;
			}
			
			return output;
    	}
    	  	
    	// Update statement in SQL database
    	public static void sqlConn(String ProdLine, String Station, String values)
    	{
    		String sql = "Update OPCReadData Set Value = @Value, DateSent = @Date Where Line = @ProdLine and Station = @Station";
			
    		using (SqlConnection conn = new SqlConnection(connectionString))
        	{
    			conn.Open();
    			using (SqlCommand cmd = new SqlCommand(sql, conn)){
    				cmd.Parameters.AddWithValue("@ProdLine", ProdLine);
    				cmd.Parameters.AddWithValue("@Station", Station);
    				cmd.Parameters.AddWithValue("@Value", values);
    				cmd.Parameters.AddWithValue("@Date", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
	    			cmd.ExecuteNonQuery();
    			}
    		}
    	}
    	
    	// Track EL Images statement in SQL database
    	public static void sqlELImageTracker(String SN)
    	{
    		String sql = "Select Top 1 SN From EOLELImageChecker Order By ID Desc";
    		String result = "";
    		
    		using (SqlConnection conn = new SqlConnection(connectionString))
        	{
    			conn.Open();
    			using (SqlCommand cmd = new SqlCommand(sql, conn))				
    			using (SqlDataReader reader = cmd.ExecuteReader())
                {
    				while (reader.Read())
					{
    					// get the results of each column
    					result = String.Format("{0}", reader[0]);
    				}
    			}
    		}
    		
    		if(result != SN){    		
	    		sql = "Insert Into EOLELImageChecker (SN, ImageFound, RecordTime) Values(@SN, 0, @Date)";
	    		
	    		using (SqlConnection conn = new SqlConnection(connectionString))
	        	{
	    			conn.Open();
	    			using (SqlCommand cmd = new SqlCommand(sql, conn)){
	    				cmd.Parameters.AddWithValue("@SN", SN);
	    				cmd.Parameters.AddWithValue("@Date", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
		    			cmd.ExecuteNonQuery();
	    			}
	    		}
    		}
    	}
    	
    	// Update statement in SQL database
    	public static void sqlAMinus(String ProdLine, String Station, String values, String Remark)
    	{
    		String sql = "Insert Into QualityJudge (SN, ModuleClass, TimeStamp, Operator, Remark, Station, Line) " +
    			"Values(@Value, 'A-', @Date, @ProdLine, @Remark, @Station, @ProdLine)";
			
    		using (SqlConnection conn = new SqlConnection(connectionString))
        	{
    			conn.Open();
    			using (SqlCommand cmd = new SqlCommand(sql, conn)){
    				cmd.Parameters.AddWithValue("@ProdLine", ProdLine);
    				cmd.Parameters.AddWithValue("@Station", Station);
    				cmd.Parameters.AddWithValue("@Value", values);    				
    				cmd.Parameters.AddWithValue("@Remark", Remark);
    				cmd.Parameters.AddWithValue("@Date", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
	    			cmd.ExecuteNonQuery();
    			}
    		}
    	}
    	
    	// Select statement in SQL database
    	public static string sqlGetClass(string sn)
    	{
    		String sql = "Select Top 1 ModuleClass From QualityJudge Where SN = '" + sn + "' Order By Timestamp Desc";
    		String result = "";
    		
    		using (SqlConnection conn = new SqlConnection(connectionString))
        	{
    			conn.Open();
    			using (SqlCommand cmd = new SqlCommand(sql, conn))				
    			using (SqlDataReader reader = cmd.ExecuteReader())
                {
    				while (reader.Read())
					{
    					// get the results of each column
    					result = String.Format("{0}", reader[0]);
    				}
    			}
    		}    		
    		return result;
    	}

		// Select statement in SQL database
    	public static string sqlGetRemark(string sn)
    	{
    		String sql = "Select Top 1 Remark From QualityJudge Where SN = '" + sn + "' Order By Timestamp Desc";
    		String result = "";
    		
    		using (SqlConnection conn = new SqlConnection(connectionString))
        	{
    			conn.Open();
    			using (SqlCommand cmd = new SqlCommand(sql, conn))				
    			using (SqlDataReader reader = cmd.ExecuteReader())
                {
    				while (reader.Read())
					{
    					// get the results of each column
    					result = String.Format("{0}", reader[0]);
    				}
    			}
    		}    		
    		return result;
    	}
    	
    	public static string sqlGetData(string sn)
    	{
    		String sql = "SELECT top 1 [Pmax] FROM [ElecParaTest] where Fnumber = '" + sn + "'";
    		String result = "";
    		
    		try{
	    		using (SqlConnection conn = new SqlConnection(connectionStringNP))
	        	{
	    			conn.Open();
	    			using (SqlCommand cmd = new SqlCommand(sql, conn))				
	    			using (SqlDataReader reader = cmd.ExecuteReader())
	                {
	    				while (reader.Read())
						{
	    					// get the results of each column
	    					result = String.Format("{0}", reader[0]);
	    				}
	    			}
	    		}    		
	    		return result;
    		}
    		catch (Exception){
    			Library.WriteErrorLog("Failed");
    			return "";
    		}
    	}
    	
    	public static string getIdealPower(string actualPower, string sModuleNo, int SchemeInterID = 0, string sqlConn = "") //Modify by Gengao 2012-09-05
        {            
    		string idealPower = "-1"; //Modify by Gengao 2012-09-05
            string sqlGetPmaxID = "";
            if (SchemeInterID == 0) //Modify by Gengao 2012-09-05
                sqlGetPmaxID = "select a.Pmax,a.MinValue,a.CtlValue,a.MaxValue,a.RESV1 FROM SchemeEntry as a where a.PmaxID=(select ItemPmaxID from Scheme where InterID=(select distinct SchemeInterID from ElecParaTest where FNumber='" + sModuleNo + "' and InterID in (select max(InterID) from ElecParaTest where FNumber='" + sModuleNo + "')))";
            else
                sqlGetPmaxID = "select a.Pmax,a.MinValue,a.CtlValue,a.MaxValue,a.RESV1 FROM SchemeEntry as a where a.PmaxID=(select ItemPmaxID from Scheme where InterID=" + SchemeInterID + ")";
            
            string strPmax = "", strMinCtlValue = "", strMaxCtlValue = "", strCtlValue = "", strMinValue = "", strMaxValue = "";

            string strGrade = ""; // added by Gengao 2012-09-05
            
            SqlDataReader dr = null;
            
            try
            {
                dr = GetDataReader(sqlGetPmaxID);
                if (dr != null)
                {
                    while (dr.Read())
                    {
                        strPmax = dr.GetString(0).Replace("W", "");
                        strGrade = dr.GetString(4); 
                        if (!dr.GetString(1).Trim().Equals(""))
                        {
                            strMinCtlValue = dr.GetString(1).Substring(dr.GetString(1).IndexOf(']') + 1, 2).Trim();
                            strMinValue = dr.GetString(1).Substring(dr.GetString(1).IndexOf(strMinCtlValue) + 2).Trim();
                        }
                        if (!dr.GetString(3).Trim().Equals(""))
                        {
                            strMaxCtlValue = dr.GetString(3).Substring(dr.GetString(3).IndexOf(']') + 1, 2).Trim();
                            strMaxValue = dr.GetString(3).Substring(dr.GetString(3).IndexOf(strMaxCtlValue) + 2).Trim();
                        }
                        strCtlValue = dr.GetString(2).Trim();
                        if (CheckIdealPower(actualPower, strMinCtlValue, strMaxCtlValue, strCtlValue, strMinValue, strMaxValue))
                        {
                            //Modify by Gengao 2012-09-05
                            if (strGrade.ToString().Trim().ToUpper() == "")
                                idealPower = strPmax;
                            else
                                idealPower = strPmax + "_"+ strGrade;
                            break;
                        }
                        strPmax = ""; strMinCtlValue = ""; strMaxCtlValue = ""; strCtlValue = ""; strMinValue = ""; strMaxValue = "";
                        strGrade = "";//added by Gengao 2012-09-05
                    }
                    dr.Close();
                    dr = null;
                }
            }
            catch (Exception ex)
            {
                dr.Close();
                dr = null;
            }
            return idealPower;
        }
    	
    	public static SqlDataReader GetDataReader(string SqlStr)
        {
            SqlDataReader dr = null;
            System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(connectionStringNP);
            using (System.Data.SqlClient.SqlCommand cmd = conn.CreateCommand())
            {                  
                    try
                    {
                        cmd.CommandText = SqlStr;
                        cmd.CommandType = CommandType.Text;

                        if (conn.State == ConnectionState.Closed)
                            conn.Open();
                        dr = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
                    }
                    catch (Exception ex)
                    {
                        dr = null;
                    }
             }
            return dr;
        }
    	
    	public static bool CheckIdealPower(string strPmax, string strMinCtlValue, string strMaxCtlValue, string strCtlValue, string strMinValue, string strMaxValue)
        {
            bool idearPower = false;
            bool MinCheck = false, MaxCheck=false;

            if (!strMinValue.Equals(""))
            {
                if (strMinCtlValue.Equals("="))
                {
                    if (Convert.ToDouble(strPmax) == Convert.ToDouble(strMinValue))
                        MinCheck = true;
                    else
                        MinCheck = false;
                }
                else if (strMinCtlValue.Equals(">"))
                {
                    if (Convert.ToDouble(strPmax) > Convert.ToDouble(strMinValue))
                        MinCheck = true;
                    else
                        MinCheck = false;
                }
                else
                {
                    if (Convert.ToDouble(strPmax) >= Convert.ToDouble(strMinValue))
                        MinCheck = true;
                    else
                        MinCheck = false;
                }
            }
            else
                MinCheck = false;

            if (!strMaxValue.Equals(""))
            {
                if (strMaxCtlValue.Equals("="))
                {
                    if (Convert.ToDouble(strPmax) == Convert.ToDouble(strMaxValue))
                        MaxCheck = true;
                    else
                        MaxCheck = false;
                }
                else if (strMaxCtlValue.Equals("<"))
                {
                    if (Convert.ToDouble(strPmax) < Convert.ToDouble(strMaxValue))
                        MaxCheck = true;
                    else
                        MaxCheck = false;
                }
                else
                {
                    if (Convert.ToDouble(strPmax) <= Convert.ToDouble(strMaxValue))
                        MaxCheck = true;
                    else
                        MaxCheck = false;
                }
            }
            else
                MaxCheck = false;

            if (strCtlValue.Equals("And"))
            {
                if (MaxCheck && MinCheck)
                    idearPower = true;
            }
            else
            {
                if (MaxCheck || MinCheck)
                    idearPower=true;
            }
            return idearPower;
        }
    }
}