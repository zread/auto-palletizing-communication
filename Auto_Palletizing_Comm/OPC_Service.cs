﻿/* 
 * User: Zach Read
 * Date Create: 25-08-15
 * Date Modified: 07-01-16
 * Purpose: This method manages the service. It creats a timer event that controls how often the RunScript class is called.
 * This windows service manages the communication between the packaging system and the PLC through communication with the OPC server.
 * 
 */

using System;
using System.ServiceProcess;
using System.Timers;
using System.Threading;

namespace Auto_Palletizing_Comm
{
    public partial class OPC_Service : ServiceBase
    {
        private System.Timers.Timer timer1 = null;

        public OPC_Service()
        {
            InitializeComponent();
        }

        // Is started when the service starts
        protected override void OnStart(string[] args)
        {
            Library.WriteErrorLog("Service has started");
            
            timer1 = new System.Timers.Timer();
            this.timer1.Interval = 2000;       // 2 seconds
            this.timer1.Elapsed += new System.Timers.ElapsedEventHandler(this.timer1_Tick);
            timer1.Enabled = true;
        }

        // Runs every time there is a timer event
        private void timer1_Tick(object sender, ElapsedEventArgs e)
        {
        	if(Monitor.TryEnter(timer1))
        	{
        		try{
        			//OPC.RunScript();
        			OPC.ReadData();
        		}
        		finally{
        			Monitor.Exit(timer1);
        		}
        	}        	
        }

        // Runs when the serrvice is stopped
        protected override void OnStop()
        {
            timer1.Enabled = false;
            Library.WriteErrorLog("Service has stopped running");
        }
    }
}