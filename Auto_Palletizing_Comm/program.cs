﻿/*
 * Created by SharpDevelop.
 * User: zach.read
 * Date: 03-02-16
 * Time: 10:29
 * 
 */
 
using System;
using System.Linq;
using System.ServiceProcess;

namespace Auto_Palletizing_Comm
{
	/// <summary>
	/// Description of program.
	/// </summary>
	public class program
	{
		 /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new OPC_Service()
            };
            ServiceBase.Run(ServicesToRun);
        }
	}
}
