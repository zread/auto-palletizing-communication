﻿/*
 * Created by SharpDevelop.
 * User: Zach.Read
 * Date: 03-02-16
 * Time: 11:11
 * 
 */
using System;
using System.ComponentModel;
using System.Linq;


namespace Auto_Palletizing_Comm
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : System.Configuration.Install.Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
        }
    }
}
