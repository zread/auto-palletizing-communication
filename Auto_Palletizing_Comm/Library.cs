﻿/*
 * Created by SharpDevelop.
 * User: zach.read
 * Date: 03-02-16
 * Time: 09:29
 * 
 */

using System;
using System.IO;

namespace Auto_Palletizing_Comm
{
    class Library
    {
    	// Sets the line
    	//private static string line = "B";
    	
        public static void WriteErrorLog(Exception ex)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
                sw.WriteLine(GetTimestamp(DateTime.Now) + ": " + ex.Source.ToString().Trim() + "; " + ex.Message.ToString().Trim());
                sw.Flush();
                sw.Close();
            }
            catch
            {
            }
        }
        
        public static void WriteLastSN(string sn)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\Tracker.txt", false);
                sw.WriteLine(sn);
                sw.Flush();
                sw.Close();
            }
            catch
            {
            }
        }
        
        public static void WriteLastSNAppend(string sn)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\Tracker.txt", true);
                sw.WriteLine(sn);
                sw.Flush();
                sw.Close();
            }
            catch
            {
            }
        }

        public static void WriteErrorLog(String message)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
                sw.WriteLine(GetTimestamp(DateTime.Now) + ": " + message);
                sw.Flush();
                sw.Close();
            }
            catch
            {
            }
        }

        public static String GetTimestamp(DateTime value)
        {
            return value.ToString("dd'/'MM'/'yy HH:mm:ss:ffff");
        }

    }
}
